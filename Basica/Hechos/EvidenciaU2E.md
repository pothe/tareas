# Evidencia de Aprendizaje

Los negocios en México necesitan empleados que se encargan de realizar actividades asignadas para que el negocio funcione correctamente y así puedan ofrecer sus productos y servicios para satisfacer las necesidades de sus respectivos clientes.

No todos los negocios son iguales ya que algunos no necesitan muchas personas para operar correctamente aunque algunos negocios operan tienen muy pocas personas en su organización mientras que otros negocios pueden tener más personal del que requieren.

Es por eso que se analizara por medio de la Encuesta Nacional de Micronegocios realizada por el Instituto Nacional de Geografía y Estadística, el número de empleados que tienen los pequeños negocios en México que se encuentran clasificados.

Siendo así que con ayuda de la Estadística se podrá identificar cuál es el promedio de empleados que necesita un negocio además de conocer los empleados que son necesarios en los distintos sectores.
